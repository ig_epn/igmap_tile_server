'''
Created on Jul 4, 2023
@author: wacero
'''
import io
import sqlite3
from flask import request, current_app, send_file, abort
from werkzeug.security import generate_password_hash, check_password_hash
from . import tile_server_app 
import flask

##@tile_server_app.route('/tiles/<int:z>/<int:x>/<int:y>.png') # type: ignore

@tile_server_app.route('/tiles')
def osm_server_tile():

    security_token = generate_password_hash(current_app.config['ACCESS_TOKEN'])
    tiles_database_path = current_app.config['TILES_DATABASE_PATH']

    token = request.args.get('token')
    x = request.args.get('x')
    y = request.args.get('y')
    z = request.args.get('z')
    
    if token and check_password_hash(security_token, token):
        print("Token checked")
        conn = sqlite3.connect(tiles_database_path)
        c = conn.cursor()
        c.execute('SELECT tile_data FROM tiles WHERE zoom_level=? AND tile_column=? AND tile_row=?',
	            (z, x, y))
        result = c.fetchone()

        if result:
            conn.close()
            return send_file(io.BytesIO(result[0]), mimetype='image/png')
        else:
            conn.close()
            flask.abort(404)
        
        
    else:
        return flask.abort(403)
    
        


