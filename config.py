import os
import json

basedir = os.path.abspath(os.path.dirname(__file__))

with open("%s/data/igmap_tile_server_info.json" %basedir,'r') as json_file:
    tile_server_info = json.load(json_file)


class Config:

    SECRET_KEY = os.environ.get('SECRET_KEY')
    SERVICE_USER = tile_server_info['user']
    ACCESS_TOKEN = tile_server_info['access_password'] or os.environ.get('ACCESS_TOKEN') 
    TILES_DATABASE_PATH = tile_server_info['tile_map_path'] or '/opt/osm/world_10_1.3.mbtiles'
    
    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True

class ProductionConfig(Config): 

    DEBUG = False

config = {
        'development': DevelopmentConfig,
        'production': ProductionConfig, 

        'default': DevelopmentConfig

            }
