import os

from igmap_tile_server import create_igmap_app

app = create_igmap_app(os.getenv('FLASK_CONFIG') or 'default')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=36001)


