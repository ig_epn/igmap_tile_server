# IGmap Tile Server

This repository contains the code for the IGmap Tile Server.


```
igmap_tile_server
|
|--igmap_tile_server
    |
    |--__init__.py 
    |-- igmap_tile_server_app
        |
        |-- __init__.py
        |-- views.py 

|--config.py
|--manage.py

```

## Files

- `igmap_tile_server`: Main folder
- `config.py`: Set the configuration for the service
- `manage.py`: Run the code with the development server


## Usage

Provide instructions on how to use or install the project.

## Contributing

Provide instructions on how contributors can contribute to this project.

## License

Specify the license for the project.

## Contact

Provide contact information for the maintainers of the project.
